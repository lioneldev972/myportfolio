<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardsController extends AbstractController
{
    /**
     * @Route("/cards", name="cards")
     */
    public function index(): Response
    {
        return $this->render('admin/cards.html.twig', [
            'controller_name' => 'CardsController',
        ]);
    }
}
