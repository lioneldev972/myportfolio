<?php

namespace App\Controller\Admin;

use App\Entity\Techno;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    public function __construct(){
        //$this->session = $session;
    }

    /**
     * @Route("/admin", name="road_admin")
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */
    public function index(SessionInterface $session, Request $request): Response
    {



       // $user = $session->get("user",[]);
        //Par défaut
        //return parent::index();


        $user = $this->getUser();

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $result = $userRepository->findUser($user->getUsername());

        $user = [
            'firstname' => $result->getFirstname(),
            'name' => $result->getNameUser(),
            'mail' => $result->getMailUser()
        ];

        $session->set("user",$user);

        //$routeBuilder = $this->get(CrudUrlGenerator::class)->build();
        //return  $this->redirect($routeBuilder->setController(TechnoCrudController::class)->generateUrl());
        // you can also render some template to display a proper Dashboard
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        return $this->render('admin/index.html.twig',
       [
            'user' => $user["firstname"],
            'name' =>  $user["name"],
            'mail' =>  $user["mail"]

        ]
        );
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Myportfolio');
    }

    public function configureMenuItems(): iterable
    {
        return[ 
            yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),
            yield MenuItem::linkToCrud('Techno', 'fa fa-user', Techno::class)
            //yield MenuItem::linkToCrud('Techno', 'fa fa-user', Techno::class)
            //yield MenuItem::linkToCrud('Techno', 'fa fa-user', Techno::class)

        ];
    }
}
