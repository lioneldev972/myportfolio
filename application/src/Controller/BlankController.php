<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class BlankController extends AbstractController
{
    public function __construct(){

    }

    /**
     * @Route("/blank", name="blank")
     * @param SessionInterface $session
     * @return Response
     */
    public function index(SessionInterface $session): Response
    {

        dump($session);
        return $this->render('admin/blank.html.twig', [
            'controller_name' => 'BlankController',
        ]);
    }
}
