<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ButtonsFormController extends AbstractController
{
    /**
     * @Route("/buttons", name="buttons")
     */
    public function index(): Response
    {
        return $this->render('admin/buttons.html.twig', [
            'controller_name' => 'ButtonsFormController',
        ]);
    }
}
