<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private  $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
         $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setNameUser("TaichoLarson");
        $user->setFirstname('Lionel');
        $user->setPassword($this->passwordEncoder->encodePassword($user,'jerentre753'));
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setMailUser("admin@admin.com");
        $manager->persist($user);

        $manager->flush();
    }
}
