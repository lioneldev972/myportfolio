<?php

namespace App\Entity;

use App\Repository\TechnoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TechnoRepository::class)
 */
class Techno
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $nomTechno;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $iconTechno;

    /**
     * @ORM\ManyToMany(targetEntity=Project::class, inversedBy="idTechno")
     */
    private $idProject;

    public function __construct()
    {
        $this->idProject = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomTechno(): ?string
    {
        return $this->nomTechno;
    }

    public function setNomTechno(string $nomTechno): self
    {
        $this->nomTechno = $nomTechno;

        return $this;
    }

    public function getIconTechno(): ?string
    {
        return $this->iconTechno;
    }

    public function setIconTechno(string $iconTechno): self
    {
        $this->iconTechno = $iconTechno;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getIdProject(): Collection
    {
        return $this->idProject;
    }

    public function addIdProject(Project $idProject): self
    {
        if (!$this->idProject->contains($idProject)) {
            $this->idProject[] = $idProject;
        }

        return $this;
    }

    public function removeIdProject(Project $idProject): self
    {
        $this->idProject->removeElement($idProject);

        return $this;
    }
}
