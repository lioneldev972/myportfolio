<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $titleproject;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionproject;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkgit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linksite;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="idProject", orphanRemoval=true)
     */
    private $idImage;

    /**
     * @ORM\OneToMany(targetEntity=Update::class, mappedBy="idProject")
     */
    private $idUpdate;

    /**
     * @ORM\ManyToMany(targetEntity=Techno::class, mappedBy="idProject")
     */
    private $idTechno;

    public function __construct()
    {
        $this->idImage = new ArrayCollection();
        $this->idUpdate = new ArrayCollection();
        $this->idTechno = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleproject(): ?string
    {
        return $this->titleproject;
    }

    public function setTitleproject(string $titleproject): self
    {
        $this->titleproject = $titleproject;

        return $this;
    }

    public function getDescriptionproject(): ?string
    {
        return $this->descriptionproject;
    }

    public function setDescriptionproject(?string $descriptionproject): self
    {
        $this->descriptionproject = $descriptionproject;

        return $this;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getLinkgit(): ?string
    {
        return $this->linkgit;
    }

    public function setLinkgit(?string $linkgit): self
    {
        $this->linkgit = $linkgit;

        return $this;
    }

    public function getLinksite(): ?string
    {
        return $this->linksite;
    }

    public function setLinksite(?string $linksite): self
    {
        $this->linksite = $linksite;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getIdImage(): Collection
    {
        return $this->idImage;
    }

    public function addIdImage(Image $idImage): self
    {
        if (!$this->idImage->contains($idImage)) {
            $this->idImage[] = $idImage;
            $idImage->setIdProject($this);
        }

        return $this;
    }

    public function removeIdImage(Image $idImage): self
    {
        if ($this->idImage->removeElement($idImage)) {
            // set the owning side to null (unless already changed)
            if ($idImage->getIdProject() === $this) {
                $idImage->setIdProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Update[]
     */
    public function getIdUpdate(): Collection
    {
        return $this->idUpdate;
    }

    public function addIdUpdate(Update $idUpdate): self
    {
        if (!$this->idUpdate->contains($idUpdate)) {
            $this->idUpdate[] = $idUpdate;
            $idUpdate->setIdProject($this);
        }

        return $this;
    }

    public function removeIdUpdate(Update $idUpdate): self
    {
        if ($this->idUpdate->removeElement($idUpdate)) {
            // set the owning side to null (unless already changed)
            if ($idUpdate->getIdProject() === $this) {
                $idUpdate->setIdProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Techno[]
     */
    public function getIdTechno(): Collection
    {
        return $this->idTechno;
    }

    public function addIdTechno(Techno $idTechno): self
    {
        if (!$this->idTechno->contains($idTechno)) {
            $this->idTechno[] = $idTechno;
            $idTechno->addIdProject($this);
        }

        return $this;
    }

    public function removeIdTechno(Techno $idTechno): self
    {
        if ($this->idTechno->removeElement($idTechno)) {
            $idTechno->removeIdProject($this);
        }

        return $this;
    }
}
