<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201219140403 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, id_project_id INT NOT NULL, id_image INT NOT NULL, name_image VARCHAR(150) NOT NULL, url_image VARCHAR(255) DEFAULT NULL, INDEX IDX_C53D045FB3E79F4B (id_project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, titleproject VARCHAR(150) NOT NULL, descriptionproject LONGTEXT DEFAULT NULL, datecreation DATETIME NOT NULL, linkgit VARCHAR(255) DEFAULT NULL, linksite VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE techno (id INT AUTO_INCREMENT NOT NULL, nom_techno VARCHAR(150) NOT NULL, icon_techno VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE techno_project (techno_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_F652711D51F3C1BC (techno_id), INDEX IDX_F652711D166D1F9C (project_id), PRIMARY KEY(techno_id, project_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `update` (id INT AUTO_INCREMENT NOT NULL, id_project_id INT NOT NULL, id_user_id INT NOT NULL, date DATETIME NOT NULL, commentary LONGTEXT DEFAULT NULL, INDEX IDX_98253578B3E79F4B (id_project_id), INDEX IDX_9825357879F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, id_user INT NOT NULL, name_user VARCHAR(255) NOT NULL, mail_user VARCHAR(255) NOT NULL, pass_user VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045FB3E79F4B FOREIGN KEY (id_project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE techno_project ADD CONSTRAINT FK_F652711D51F3C1BC FOREIGN KEY (techno_id) REFERENCES techno (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE techno_project ADD CONSTRAINT FK_F652711D166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `update` ADD CONSTRAINT FK_98253578B3E79F4B FOREIGN KEY (id_project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE `update` ADD CONSTRAINT FK_9825357879F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045FB3E79F4B');
        $this->addSql('ALTER TABLE techno_project DROP FOREIGN KEY FK_F652711D166D1F9C');
        $this->addSql('ALTER TABLE `update` DROP FOREIGN KEY FK_98253578B3E79F4B');
        $this->addSql('ALTER TABLE techno_project DROP FOREIGN KEY FK_F652711D51F3C1BC');
        $this->addSql('ALTER TABLE `update` DROP FOREIGN KEY FK_9825357879F37AE5');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE techno');
        $this->addSql('DROP TABLE techno_project');
        $this->addSql('DROP TABLE `update`');
        $this->addSql('DROP TABLE user');
    }
}
